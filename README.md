# Configurações do projeto #

Segue as configurações para conexão com banco de dados e endpoints
para realizar as requisições do mesmo. 

### Configurações para banco postgrees ###

* spring.datasource.url=jdbc:postgresql://localhost:5432/program
* spring.datasource.username=postgres
* spring.datasource.password=1234 

obs: O nome do banco é program, e  utilizei o admin padrão do postgres que é "postgres"
e a senha do mesmo 1234. (Essas configurações estão também no application.properties)

### Endpoints para teste ###

* baseUrl: localhost:8080/api/v1
* listagem de todos os programas: localhost:8080/api/v1/programas (verbo HTTP GET)
* retorno de um único programa por ID: localhost:8080/api/v1/programas/{id} (Verbo HTTP GET)
* Cadastro de um novo programa: localhost:8080/api/v1 (verbo HTTP POST)
* Exclusão de programa: localhost:8080/api/v1/programas/{id} (verbo HTTP DELETE)
* Download de pdf: localhost:8080/api/v1/programas/export (verbo HTTP GET)
* Foi utilizado o postman como cliente para requisição HTTP da api. 

### Gerenciador de dependencias Maven ###

* As dependencias do arquivo estão declaradas no arquivo pom.xml
