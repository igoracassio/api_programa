package com.api.crosswork.application.converters;

import com.api.crosswork.application.dtos.ProgramaDTO;
import com.api.crosswork.application.models.Programa;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProgramaConverter extends ModelMapper{

    @Autowired
    private ModelMapper modelMapper;

    public Programa transformDtoToEntity(ProgramaDTO programaDTO){
        Programa programa = new Programa();
        programa = this.modelMapper.map(programaDTO, Programa.class);
        return programa;
    }

    public ProgramaDTO transformEntityToDto(Programa programa){
        ProgramaDTO programaDTO = new ProgramaDTO();
        programaDTO = this.modelMapper.map(programa, ProgramaDTO.class);
        return programaDTO;
    }

    public List<Programa> transformDtoListToEntityList(List<ProgramaDTO> dtoList){
        return dtoList.stream().map(programaDTO -> transformDtoToEntity(programaDTO))
                .collect(Collectors.toList());
    }

    public List<ProgramaDTO> transformEntityListToDtoList(List<Programa> programaList){
        return programaList.stream().map(programa -> transformEntityToDto(programa))
                .collect(Collectors.toList());
    }
}
