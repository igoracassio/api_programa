package com.api.crosswork.application.controllers;

import com.api.crosswork.application.converters.ProgramaConverter;
import com.api.crosswork.application.dtos.ProgramaDTO;
import com.api.crosswork.application.models.Programa;
import com.api.crosswork.application.services.ProgramaService;
import com.api.crosswork.application.utils.ProgramaPDFExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(value = "*")
public class ProgramaController {

    @Autowired
    private ProgramaService programaService;

    @Autowired
    private ProgramaConverter programaConverter;

    @GetMapping("/programas")
    public ResponseEntity<List<ProgramaDTO>> getAllProgramas(){

        try {
            List<Programa> programaList = this.programaService.findAll();
            List<ProgramaDTO> programaDTOList = this.programaConverter.transformEntityListToDtoList(programaList);

            if(programaDTOList.size() == 0){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(programaDTOList);
            }else {
                return ResponseEntity.ok(programaDTOList);
            }

        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @GetMapping("/programas/{id}")
    public ResponseEntity<ProgramaDTO> getProgramaById(@PathVariable(value = "id") Long id){

        try {

            Optional<Programa> opt = this.programaService.findById(id);
            ProgramaDTO programaDTO = this.programaConverter.transformEntityToDto(opt.get());
            return ResponseEntity.ok(programaDTO);

        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

    }

    @PostMapping
    public ResponseEntity<ProgramaDTO> createPrograma(@Valid @RequestBody ProgramaDTO programaDTO){

        try {

            Programa programa = new Programa();
            programa = this.programaConverter.transformDtoToEntity(programaDTO);
            programa = this.programaService.save(programa);
            ProgramaDTO dto = this.programaConverter.transformEntityToDto(programa);

            return ResponseEntity.ok(dto);

        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(programaDTO);
        }

    }

    @DeleteMapping("/programas/{id}")
    public ResponseEntity<String> deletePrograma(@PathVariable(value = "id") Long id){
        try {

            Optional<Programa> opt = this.programaService.findById(id);
            Programa programa = opt.get();
            this.programaService.delete(programa.getId());
            String response = "Programa de id: " + id + " deletado com sucesso!";
            return ResponseEntity.ok(response);

        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

    }

    @GetMapping("/programas/export")
    public void exportToPdf(HttpServletResponse response) throws IOException {

        try {
            response.setContentType("application/pdf");

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateTime = dateFormat.format(new Date());

            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=programas_"+ currentDateTime + ".pdf";

            response.setHeader(headerKey, headerValue);

            List<Programa> programaList = this.programaService.findAll();

            ProgramaPDFExporter exporter = new ProgramaPDFExporter(programaList);
            exporter.export(response);

        } catch (Exception e){
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
