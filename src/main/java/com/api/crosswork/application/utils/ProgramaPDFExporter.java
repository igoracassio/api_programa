package com.api.crosswork.application.utils;

import com.api.crosswork.application.models.Programa;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ProgramaPDFExporter {

    private List<Programa> programaList;

    public ProgramaPDFExporter(List<Programa> programaList) {
        this.programaList = programaList;
    }

    private void writeTableData(PdfPTable table){
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase("idUnidade"));

        table.addCell(cell);

        cell.setPhrase(new Phrase("sigla de unidade"));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Descrição completa"));
        table.addCell(cell);

        cell.setPhrase(new Phrase("percentual"));
        table.addCell(cell);
    }

    public void export(HttpServletResponse response) throws IOException {
        Document document = new Document(PageSize.A4);

        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();

        document.add(new Paragraph("Lista de todos os programas"));

        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100);
        writeTableData(table);
        document.add(table);

        document.close();

    }
}
