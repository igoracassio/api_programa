package com.api.crosswork.application.servicesImpl;

import com.api.crosswork.application.models.Programa;
import com.api.crosswork.application.repositories.ProgramaRepository;
import com.api.crosswork.application.services.ProgramaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProgramaServiceImpl implements ProgramaService {

    @Autowired
    private ProgramaRepository programaRepository;

    @Override
    public Programa save(Programa programa) {
        return this.programaRepository.save(programa);
    }

    @Override
    public Optional<Programa> findById(Long id) {
        return this.programaRepository.findById(id);
    }

    @Override
    public List<Programa> findAll() {
        return this.programaRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        this.programaRepository.deleteById(id);
    }
}
