package com.api.crosswork.application.dtos;

import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ProgramaDTO {

    private long id;

    private long idUnidade;

    private String codigoUnidade;

    private String descricaoUnidade;

    private String descricaoSigla;

    private Date dataLastrec;

    private String codigoUsuario;

    private String descricaoCompleta;

    private String flagPercentual;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(long idUnidade) {
        this.idUnidade = idUnidade;
    }

    public String getCodigoUnidade() {
        return codigoUnidade;
    }

    public void setCodigoUnidade(String codigoUnidade) {
        this.codigoUnidade = codigoUnidade;
    }

    public String getDescricaoUnidade() {
        return descricaoUnidade;
    }

    public void setDescricaoUnidade(String descricaoUnidade) {
        this.descricaoUnidade = descricaoUnidade;
    }

    public String getDescricaoSigla() {
        return descricaoSigla;
    }

    public void setDescricaoSigla(String descricaoSigla) {
        this.descricaoSigla = descricaoSigla;
    }

    public Date getDataLastrec() {
        return dataLastrec;
    }

    public void setDataLastrec(Date dataLastrec) {
        this.dataLastrec = dataLastrec;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getDescricaoCompleta() {
        return descricaoCompleta;
    }

    public void setDescricaoCompleta(String descricaoCompleta) {
        this.descricaoCompleta = descricaoCompleta;
    }

    public String getFlagPercentual() {
        return flagPercentual;
    }

    public void setFlagPercentual(String flagPercentual) {
        this.flagPercentual = flagPercentual;
    }
}
