package com.api.crosswork.application.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "programa")
public class Programa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "id_unidade")
    private long idUnidade;

    @Column(name = "codigo_unidade")
    private String codigoUnidade;

    @Column(name = "descricao_unidade")
    private String descricaoUnidade;

    @Column(name = "descricao_sigla")
    private String descricaoSigla;

    @Column(name = "data_lastrec")
    private Date dataLastrec;

    @Column(name = "codigo_usuario" , nullable = false)
    private String codigoUsuario;

    @Column(name = "descricao_completa")
    private String descricaoCompleta;

    @Column(name = "flag_percentual")
    private String flagPercentual;

    public Programa() {
    }

    public Programa(long id, long idUnidade, String codigoUnidade, String descricaoUnidade, String descricaoSigla, Date dataLastrec, String codigoUsuario, String descricaoCompleta, String flagPercentual) {
        this.id = id;
        this.idUnidade = idUnidade;
        this.codigoUnidade = codigoUnidade;
        this.descricaoUnidade = descricaoUnidade;
        this.descricaoSigla = descricaoSigla;
        this.dataLastrec = dataLastrec;
        this.codigoUsuario = codigoUsuario;
        this.descricaoCompleta = descricaoCompleta;
        this.flagPercentual = flagPercentual;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(long idUnidade) {
        this.idUnidade = idUnidade;
    }

    public String getCodigoUnidade() {
        return codigoUnidade;
    }

    public void setCodigoUnidade(String codigoUnidade) {
        this.codigoUnidade = codigoUnidade;
    }

    public String getDescricaoUnidade() {
        return descricaoUnidade;
    }

    public void setDescricaoUnidade(String descricaoUnidade) {
        this.descricaoUnidade = descricaoUnidade;
    }

    public String getDescricaoSigla() {
        return descricaoSigla;
    }

    public void setDescricaoSigla(String descricaoSigla) {
        this.descricaoSigla = descricaoSigla;
    }

    public Date getDataLastrec() {
        return dataLastrec;
    }

    public void setDataLastrec(Date dataLastrec) {
        this.dataLastrec = dataLastrec;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getDescricaoCompleta() {
        return descricaoCompleta;
    }

    public void setDescricaoCompleta(String descricaoCompleta) {
        this.descricaoCompleta = descricaoCompleta;
    }

    public String getFlagPercentual() {
        return flagPercentual;
    }

    public void setFlagPercentual(String flagPercentual) {
        this.flagPercentual = flagPercentual;
    }
}
