package com.api.crosswork.application.repositories;

import com.api.crosswork.application.models.Programa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProgramaRepository extends JpaRepository<Programa, Long> {

    Optional<Programa> findById(Long id);

}
