package com.api.crosswork.application.services;

import com.api.crosswork.application.models.Programa;

import java.util.List;
import java.util.Optional;

public interface ProgramaService {

    Programa save(Programa programa);

    Optional<Programa> findById(Long id);

    List<Programa> findAll();

    void delete(Long id);

}
